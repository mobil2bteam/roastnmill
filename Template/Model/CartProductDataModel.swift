
import RealmSwift

class CartProductDataModel: Object{
    dynamic var id: Int = 0
    dynamic var name: String = ""
    dynamic var count: Int = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
}



