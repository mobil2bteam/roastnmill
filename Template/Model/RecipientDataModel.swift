
import RealmSwift

class RecipientDataModel: Object{
    
    dynamic var id: Int = 0
    dynamic var date: Date = Date()
    dynamic var name: String = ""
    dynamic var phone: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}



