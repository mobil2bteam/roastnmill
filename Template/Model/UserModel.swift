
import ObjectMapper

class UserModel: Mappable {
    
    var info: UserInfoModel!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        info                         <- map["info"]
    }
    
}

class UserInfoModel: Mappable {

    var active:Bool!
    var sex:Bool!
    var verify:Bool!
    var token: String!
    var cityName: String!
    var cityId: Int!
    var email: String!
    var userName: String!
    var firstName: String!
    var lastName: String!
    var secondName: String!
    var ordersCount: Int!
    var phone: String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        active                     <- map["active"]
        sex                        <- map["sex"]
        verify                     <- map["verify"]
        token                      <- map["token"]
        cityName                   <- map["city_name"]
        cityId                     <- map["city_id"]
        email                      <- map["email"]
        userName                   <- map["user_name"]
        firstName                  <- map["first_name"]
        lastName                   <- map["last_name"]
        secondName                 <- map["second_name"]
        ordersCount                <- map["orders_count"]
        phone                      <- map["phone"]

    }
    
}
