
import ObjectMapper

class ProductModel: Mappable {

    var id: Int!
    var code: String = ""
    var article: String = ""
    var url: String = ""
    var images: [ImageModel] = []
    var name: String = ""
    var flags: [FlagModel] = []
    var rating: Int = 0
    var commentCount: Int!
    var prices: [PriceModel] = []
    var shortDescription: String = ""
    
    required init?(map: Map) {
        
    }

    func mapping(map: Map) {
        id                      <- map["id"]
        name                    <- map["name"]
        prices                  <- map["prices"]
        commentCount            <- map["commentCount"]
        rating                  <- map["rating"]
        flags                   <- map["flags"]
        images                  <- map["images"]
        code                    <- map["code"]
        article                 <- map["article"]
        url                     <- map["url"]
        shortDescription        <- map["short_description"]

    }

}
