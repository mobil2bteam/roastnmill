
enum ApplicationMode {
    case sideMenu
    case tabBar
}

struct Constants {
    static let apiKey = "123456"
    static let apiBaseUrl = "http://main.mobil2b.com/api"
    static let appMode: ApplicationMode = .sideMenu
}

