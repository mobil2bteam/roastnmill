
import UIKit
import IQKeyboardManagerSwift
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.sharedManager().enable = true
        GMSServices.provideAPIKey("AIzaSyAkN1ZAqXBNhtLR5p6T3hEeAaVvvUX9dO8")
        UIApplication.shared.statusBarStyle = .default
        StyleManager.shared.styleAppearance()
        CacheManager.updateCache()
        return true
    }
    
}

