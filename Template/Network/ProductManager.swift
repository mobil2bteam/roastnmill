
import Alamofire
import ObjectMapper

class ProductManager {
    
    static let appManager = AppManager.shared
    
    class func getProducts(with params: [String: String], successHandler: @escaping (ProductsModel?, String?) -> Void) {
        let r = ServerAPI.products(params: params)
        Alamofire.request(r.path, method: r.method, parameters: r.parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success:
                    let JSON = response.result.value as? [String: Any]
                    if let errorResponse = JSON?["error"] as? [String: Any] {
                        let error = errorResponse["message"] as! String
                        successHandler(nil, error)
                    } else {
                        print(JSON ?? "")
                        let model = Mapper<ProductsModel>().map(JSON: JSON!)!
                        successHandler(model, nil)
                    }
                case .failure(let error):
                    successHandler(nil, error.localizedDescription)
                }
        }
    }
}



