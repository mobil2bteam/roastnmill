

import Foundation
import RealmSwift

class CartManager {

    static let notificationName = Notification.Name("CartManagerNotificationIdentifier")
    
    class func addProduct(product: ProductModel, count: Int = 1) {
        if let existProduct = uiRealm.object(ofType: CartProductDataModel.self, forPrimaryKey: product.id) {
            try! uiRealm.write{
                if count == 1 {
                    existProduct.count += 1
                } else {
                    existProduct.count = count
                }
                if count == 0 {
                    uiRealm.delete(existProduct)
                }
            }
        } else {
            if count == 0 {
                return
            }
            let newProduct = CartProductDataModel()
            newProduct.id = product.id
            newProduct.name = product.name
            newProduct.count = count
            try! uiRealm.write{
                uiRealm.add(newProduct)
            }
        }
        NotificationCenter.default.post(name: notificationName, object: nil)
    }
    
    class func increaseProduct(productId: Int) {
        if let existProduct = uiRealm.object(ofType: CartProductDataModel.self, forPrimaryKey: productId) {
            try! uiRealm.write{
                existProduct.count += 1
            }
        }
        NotificationCenter.default.post(name: notificationName, object: nil)
    }

    class func decreaseProduct(productId: Int) {
        if let existProduct = uiRealm.object(ofType: CartProductDataModel.self, forPrimaryKey: productId) {
            try! uiRealm.write{
                if existProduct.count > 1 {
                    existProduct.count -= 1
                } else {
                    uiRealm.delete(existProduct)
                }
            }
        }
        NotificationCenter.default.post(name: notificationName, object: nil)
    }
    
    class func updateProduct(productId: Int, count: Int) {
        if let existProduct = uiRealm.object(ofType: CartProductDataModel.self, forPrimaryKey: productId) {
            try! uiRealm.write{
                existProduct.count = count
                if existProduct.count == 0 {
                    uiRealm.delete(existProduct)
                }
            }
        }
        NotificationCenter.default.post(name: notificationName, object: nil)
    }
    
    class func deleteProduct(product: CartProductDataModel) {
        try! uiRealm.write{
            uiRealm.delete(product)
        }
        NotificationCenter.default.post(name: notificationName, object: nil)
    }

    class func deleteProduct(productId: Int) {
        if let existProduct = uiRealm.object(ofType: CartProductDataModel.self, forPrimaryKey: productId) {
            try! uiRealm.write{
                uiRealm.delete(existProduct)
            }
            NotificationCenter.default.post(name: notificationName, object: nil)
        }
    }

    class func clearCart() {
        let products = uiRealm.objects(CartProductDataModel.self)
        if products.count > 0 {
            try! uiRealm.write{
                uiRealm.delete(products)
            }
        }
    }
    
    class func productsCount() -> Int {
        return uiRealm.objects(CartProductDataModel.self).count
    }

    class func cartParams() -> [String: String] {
        let products = uiRealm.objects(CartProductDataModel.self)
        let stringId = products.map({ (product) -> String in
            return "\(product.id)-\(product.count)"
        })
        return ["cart": stringId.joined(separator: ";")]
    }
    
    class func count(for productID: Int) -> Int {
        if let existProduct = uiRealm.object(ofType: CartProductDataModel.self, forPrimaryKey: productID) {
            return existProduct.count
        }
        return 0
    }
    
}
