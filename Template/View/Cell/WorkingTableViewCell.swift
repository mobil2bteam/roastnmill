
import UIKit

class WorkingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        dayLabel.textColor = UIColor.colorTextPrimary
        hoursLabel.textColor = UIColor.colorPrimary
    }
    
    func configure(for work: WorkModel) {
        dayLabel.text = work.name
        if work.freeDay == true {
            hoursLabel.text = "Выходной"
        } else {
            hoursLabel.text = work.hoursStart + " - " + work.hoursFinish
        }
    }
    
}
