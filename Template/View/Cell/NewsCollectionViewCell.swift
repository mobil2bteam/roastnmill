
import UIKit
import AlamofireImage

class NewsCollectionViewCell: UICollectionViewCell {

    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var promotionLabel: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        promotionLabel.textColor = UIColor.colorAccent
    }
    
    
    func configure(for item: NewsItemModel) {
        promotionLabel.isHidden = !item.promo
        dateLabel.text = item.formattedDate()
        nameLabel.text = item.name
        guard let image = item.image,
            let url =  URL(string: image.url) else {
                itemImageView.image = nil
                return
        }
        itemImageView.af_setImage(withURL: url)
    }

}
