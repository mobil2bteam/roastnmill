
import UIKit
import AlamofireImage

class ItemCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var pricesTableView: UITableView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var likeView: UIView!
    @IBOutlet weak var saleLabel: UILabel!
    @IBOutlet weak var pricesTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var flagsTableView: UITableView!

    class func itemHeight() -> CGFloat {
        let imageHeight = 150
        let spacing = 4
        var itemHeight = 8 + 8
        itemHeight = itemHeight + imageHeight + spacing
        
        let labelHeight = 33
        itemHeight = itemHeight + labelHeight + spacing

        if SettingsManager.allowLikeDislikeProducts {
            let likeDislikeViewHeight = 33
            itemHeight = itemHeight + likeDislikeViewHeight + spacing
        }
        
        if SettingsManager.pricesCount > 0 {
            let pricesHeight = 25 * SettingsManager.pricesCount
            itemHeight = itemHeight + pricesHeight
        }
        
        return CGFloat(itemHeight)
    }
    
    let identifier = String(describing: PriceTableViewCell.self)
    var item: CatalogModel? {
        didSet {
            pricesTableView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        flagsTableView.dataSource = self
        flagsTableView.register(FlagTableViewCell.nib(), forCellReuseIdentifier: "cell")
        saleLabel.backgroundColor = UIColor.colorBackgroundFlagDiscount
        saleLabel.textColor = UIColor.colorTextFlagDiscount
        likeView.isHidden = !SettingsManager.allowLikeDislikeProducts
        pricesTableView.register(PriceTableViewCell.nib(), forCellReuseIdentifier: identifier)
        pricesTableView.dataSource = self
        pricesTableViewHeightConstraint.constant = CGFloat(SettingsManager.pricesCount * 25)
        layoutIfNeeded()
    }

    @IBAction func likeButtonClicked(_ sender: Any) {
        DataManager.favoriteProduct(productId: item!.id)
        prepareLikeButton()
    }
    
    func configure(for item: CatalogModel) {
        self.item = item
        if item.prices.count == 0 {
            pricesTableViewHeightConstraint.constant = 0
            layoutIfNeeded()
        }
        prepareLikeButton()
        nameLabel.text = item.name
        if SettingsManager.showSaleOnProducts {
            if item.salePercent > 0 {
                saleLabel.isHidden = false
                saleLabel.text = "-\(item.salePercent)%"
            }
        }
        guard let image = item.image,
            let url =  URL(string: image.url) else {
                itemImageView.image = nil
                return
        }
        itemImageView.af_setImage(withURL: url)
    }
    
    func prepareLikeButton() {
        if DataManager.favoriteProductIsAdded(productId: item!.id) {
            likeButton.setImage( UIImage.init(named: "like_fill")!, for: .normal)
        } else {
            likeButton.setImage( UIImage.init(named: "like")!, for: .normal)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        saleLabel.isHidden = true
        item = nil
        flagsTableView.reloadData()
    }
    
}


extension ItemCollectionViewCell: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == pricesTableView {
            return item?.prices.count ?? 0
        } else {
            return item?.flags.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == pricesTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! PriceTableViewCell
            cell.configure(for: item!.prices[indexPath.row])
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FlagTableViewCell
            cell.configure(for: item!.flags[indexPath.row])
            cell.backgroundColor = UIColor.clear
            cell.contentView.backgroundColor = UIColor.clear
            return cell
        }
    }
}


