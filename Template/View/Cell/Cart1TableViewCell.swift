
import UIKit

class Cart1TableViewCell: CartTableViewCell {

    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        for b in [minusButton, plusButton, deleteButton] {
            b?.tintColor = UIColor.colorTextPrimary
        }
    }

    override func configure(for item: CatalogModel) {
        super.configure(for: item)
    }

    @IBAction func deleteButtonClicked(_ sender: Any) {
        CartManager.deleteProduct(productId: item!.prices.first?.id ?? 0)
    }
    
    @IBAction func plusButtonClicked(_ sender: Any) {
        CartManager.increaseProduct(productId: item!.prices.first?.id ?? 0)
    }
    
    @IBAction func minusButtonClicked(_ sender: Any) {
        CartManager.decreaseProduct(productId: item!.prices.first?.id ?? 0)
    }
}
