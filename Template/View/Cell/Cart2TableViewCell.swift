

import UIKit

class Cart2TableViewCell: CartTableViewCell {

    @IBOutlet weak var totalLabel: MainLabel!

    override func configure(for item: CatalogModel) {
        super.configure(for: item)
        if item.prices.count > 0 {
            let price = item.prices[0]
            totalLabel.text = "\(price.price * CartManager.count(for: item.id)) р."
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        totalLabel.text = ""
    }
}
