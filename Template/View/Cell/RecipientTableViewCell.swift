
import UIKit

class RecipientTableViewCell: UITableViewCell {

    @IBOutlet weak var recipientNameLabel: UILabel!
    @IBOutlet weak var recipientPhoneLabel: UILabel!
    
    var deleteHandler: (() -> ())?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureRecipient(for recipient: RecipientDataModel) {
        recipientNameLabel.text = recipient.name
        recipientPhoneLabel.text = recipient.phone
    }
    
    func configureAddress(for address: AddressDataModel) {
        recipientNameLabel.text = address.formattedAddress()
        recipientPhoneLabel.text = address.comment
    }

    @IBAction func removeRecipientButtonClicked(_ sender: Any) {
        if deleteHandler != nil {
            deleteHandler!()
        }
    }
}
