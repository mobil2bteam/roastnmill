
import UIKit

class FlagTableViewCell: UITableViewCell {

    @IBOutlet weak var flagView: UIView!
    @IBOutlet weak var flagLabel: UILabel!
    
    func configure(for flag:FlagModel) {
        flagLabel.text = flag.name
        flagLabel.textColor = UIColor(flag.colorText)
        flagView.backgroundColor = UIColor(flag.color)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        flagLabel.text = " "
    }
}
