
import UIKit
import AlamofireImage

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    func configure(for url: String) {
        if URL(string: url) != nil {
            imageView.af_setImage(withURL: URL(string: url)!)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
    }
}
