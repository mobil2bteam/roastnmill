
import UIKit
import Material

class TextField: ErrorTextField {

    dynamic var placeholderColor: UIColor! {
        get { return self.placeholderNormalColor }
        set { self.placeholderNormalColor = newValue }
    }
    
    dynamic var activePlaceholderColor: UIColor! {
        get { return self.placeholderActiveColor }
        set { self.placeholderActiveColor = newValue }
    }
    
    dynamic var lineColor: UIColor! {
        get { return self.dividerActiveColor }
        set { self.dividerActiveColor = newValue}
    }
    
    dynamic var placeholderOffset: CGFloat {
        get { return self.placeholderVerticalOffset }
        set { self.placeholderVerticalOffset = newValue}
    }
    
}
