import UIKit

class SearchViewController: UIViewController {

    var completionClosure: ((String) -> ())?
    var searchController : UISearchController!
    var textArray = SearchingManager.getValues()
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView.init()
        tableView.separatorColor = UIColor.colorBackgroundDark
        self.searchController = UISearchController(searchResultsController:  nil)
        self.searchController.searchBar.delegate = self
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.dimsBackgroundDuringPresentation = true
        self.navigationItem.titleView = searchController.searchBar
        self.definesPresentationContext = true
    }
    
    @IBAction func clearResultsButtonClicke(_ sender: Any) {
        //contentView.isHidden = true
        textArray = []
        tableView.reloadData()
        SearchingManager.clear()
    }
    
    @IBAction func closeButtonClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

extension SearchViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return textArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = textArray[indexPath.row]
        cell.textLabel?.textColor = UIColor.colorTextPrimary
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let text = textArray[indexPath.row]
        dismiss(animated: true) { [weak self] in
            if self?.completionClosure != nil {
                self?.completionClosure!(text)
            }
        }
    }
}

extension SearchViewController: UISearchBarDelegate {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let text = searchBar.text!
        self.searchController.isActive = false
        SearchingManager.addText(text: text)
        dismiss(animated: true) { [weak self] in
            if self?.completionClosure != nil {
                self?.completionClosure!(text)
            }
        }
    }
    
}

