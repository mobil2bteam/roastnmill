
import UIKit

class CartViewController: UIViewController {

    @IBOutlet weak var cartTableView: UITableView!
    @IBOutlet weak var cartTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var promoCodeLabel: UILabel!
    @IBOutlet weak var saleLabel: UILabel!
    @IBOutlet weak var summLabel: UILabel!
    @IBOutlet weak var promoTextField: TextField!
    @IBOutlet weak var cartView: UIStackView!

    let cartCellHeight = 120
    
    var cart: CartModel! {
        didSet {
            prepareView()
        }
    }
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(CartViewController.updateProductCount), name: CartManager.notificationName, object: nil)
        title = "Корзина"
        cartView.isHidden = true
        loadCart()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if cart != nil {
            cartView.isHidden = cart.products.count == 0
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: CartManager.notificationName, object: nil)
    }

    // MARK: Methods
    func prepareView() {
        reloadTableView()
        cartView.isHidden = cart.products.count == 0
        saleLabel.text = "\(cart.totalSumSale) p."
        summLabel.text = "\(cart.totalNotSale) p."
        totalLabel.text = "\(cart.total) p."
        if promoTextField.text!.isEmpty {
            promoCodeLabel.text = ""
        } else {
            promoCodeLabel.text = "Промо-код не существует или просрочен"
            if let promoCode = cart.promoCode {
                if promoCode.percent > 0 {
                    promoCodeLabel.text = "Данный промо-код дарит вам скидку на \(promoCode.percent)%"
                }
            }
        }
    }
    
    func reloadTableView() {
        cartTableView.reloadData()
        cartTableViewHeightConstraint.constant = CGFloat(cartCellHeight * cart.products.count)
        view.layoutIfNeeded()
    }
    
    func updateProductCount() {
        if CartManager.productsCount() == cart.products.count {
            reloadTableView()
        } else {
            loadCart()
        }
    }

    // MARK: Methods
    func loadCart() {
        if CartManager.productsCount() > 0 {
            let params = cartParameters()
            OptionsManager.loadCart(params: params, successHandler: { (cart, error) in
                if cart != nil {
                    self.cart = cart
                }
                if error != nil {
                    self.alert(message: error!.localizedDescription)
                }
            })
        } else {
            cartView.isHidden = true
            saleLabel.text = "0 p."
            summLabel.text = "0 p."
            totalLabel.text = "0 p."
            cartTableViewHeightConstraint.constant = 0
            view.layoutIfNeeded()
        }
    }

    func cartParameters() -> [String: String] {
        var params = CartManager.cartParams()
        if promoTextField.text!.characters.count > 0 {
            params["promocode"] = promoTextField.text!
        }
        return params
    }
    
    // MARK: Actions
    @IBAction func updateCartButtonClicked(_ sender: Any) {
        loadCart()
    }

    @IBAction func promoButtonClicked(_ sender: Any) {
        loadCart()
    }
    
    @IBAction func orderButtonClicked(_ sender: Any) {
        let vc = Router.orderDeliveryViewController(for: cart, params: cartParameters())
        let navVC = UINavigationController.init(rootViewController: vc)
        present(navVC, animated: true, completion: nil)
    }
}

extension CartViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if cart != nil {
            return cart.products.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if SettingsManager.shoppingCartVariant1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cart1TableViewCell", for: indexPath) as! Cart1TableViewCell
            cell.configure(for: cart.products[indexPath.row])
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cart2TableViewCell", for: indexPath) as! Cart2TableViewCell
            cell.configure(for: cart.products[indexPath.row])
            return cell
        }
    }
    
}


