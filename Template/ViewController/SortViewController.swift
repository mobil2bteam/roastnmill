
import UIKit
import SVProgressHUD

typealias SortBlock = ((ProductsModel) -> ())

class SortViewController: UIViewController {

    @IBOutlet weak var sortTableView: UITableView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var bottomSpacingConstraint: NSLayoutConstraint!
    
    var sortBlock: SortBlock?
    var sorts = AppManager.shared.options.directory.sorts!
    var products: ProductsModel!

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetButton.backgroundColor = UIColor.clear
        bottomSpacingConstraint.constant = 0
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func dismissTapped(_ sender: Any) {
        dismiss()
    }
    
    func dismiss() {
        bottomSpacingConstraint.constant = -250
        UIView.animate(withDuration: 0.25, animations: { [weak self] in
            self?.view.layoutIfNeeded()
        }) { [weak self] (finished) in
            if finished {
                self?.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    func loadProducts() {
        SVProgressHUD.show()
        let params = products.filter.filterParams()
        ProductManager.getProducts(with: params) { [weak self] (products, error) in
            SVProgressHUD.dismiss()
            if error != nil {
                self?.alert(message: error!)
            }
            if products != nil {
                if self?.sortBlock != nil {
                    self?.sortBlock!(products!)
                }
                self?.dismiss()
            }
        }
    }
    
    @IBAction func sortButtonClicked(_ sender: Any) {
        products.filter.sortId = 0
        sortTableView.reloadData()
        loadProducts()
    }
}

extension SortViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sorts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.tintColor = UIColor.white
        cell.selectionStyle = .none
        cell.textLabel?.font = UIFont.systemFont(ofSize: 14)
        let sort = sorts[indexPath.row]
        cell.textLabel?.text = sort.name
        if sort.id == products.filter.sortId {
            cell.accessoryType = .checkmark
            cell.backgroundColor = UIColor.colorPrimary
            cell.textLabel?.textColor = UIColor.white
        } else {
            cell.backgroundColor = UIColor.white
            cell.textLabel?.textColor = UIColor.colorTextPrimary
            cell.accessoryType = .none
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        products.filter.sortId = sorts[indexPath.row].id
        tableView.reloadData()
        loadProducts()
    }
}

