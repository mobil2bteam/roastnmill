
import UIKit

class OrderFinishViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isNavigationBarHidden = true
        view.backgroundColor = UIColor.colorPrimary
        CartManager.clearCart()
    }


    @IBAction func okButtonClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
