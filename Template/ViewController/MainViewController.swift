
import UIKit
import ObjectMapper
import ImageSlideshow

class MainViewController: BaseViewController {
    @IBOutlet var slideshow: ImageSlideshow!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet weak var socialView: UIView!
    @IBOutlet weak var deliveryView: UIView!
    @IBOutlet weak var recentViewedView: UIView!

    var refreshControl: UIRefreshControl!
    let menuSegue = "embeddedMenuSegue"
    let groupSegue = "embeddedGroupSegue"
    let recentViewedSegue = "embeddedRecentViewedSegue"
    var menuVC: MenuViewController!
    var groupVC: GroupViewController!
    var recentViewedVC: RecentViewedViewController!

    //***************************************************
    // MARK: - Lifecycle -
    //***************************************************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareView()
        if Constants.appMode == .tabBar {
            navigationItem.leftBarButtonItem = nil
        }
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(updateHome), for: .valueChanged)
        scrollView.refreshControl = refreshControl
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MainViewController.didTapSlider))
        slideshow.addGestureRecognizer(gestureRecognizer)
    }
    
    func didTapSlider() {
        if let link = home.slider!.items[slideshow.currentPage].link {
            if link.name != nil {
                OptionsManager.request(for: link, fromController: self)
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Главная"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        title = ""
    }
    
    func updateHome() {
        OptionsManager.loadOptions(useCache: false).then {
            options in
            AppManager.shared.options = options
            }.catch { [weak self] error in
                self?.alert(message: error.localizedDescription)
            }.always {
                self.prepareView()
                self.refreshControl.endRefreshing()
        }
    }

    //***************************************************
    // MARK: - Preparation view -
    //***************************************************

    func prepareView() {
        prepareSliderView()
        menuVC.menu = home.menu!
        menuVC.menuCollectionView.reloadData()
        
        groupVC.groups = home.groups
        groupVC.groupsTableView.reloadData()
        
        socialView.isHidden = !SettingsManager.showSocialLinks
        deliveryView.isHidden = !SettingsManager.showDeliveryCity
        recentViewedView.isHidden = !SettingsManager.showWatchedProducts
    }

    func prepareSliderView() {
        if let slider = home.slider {
            if slider.items.count > 0 {
                // Add aspect ratio to slider
                let multiplier = slider.items[0].image.aspectRatio
                slideshow.heightAnchor.constraint(equalTo: slideshow.widthAnchor, multiplier: multiplier).isActive = true
                DispatchQueue.main.async { [weak self] in
                    self?.view.layoutIfNeeded()
                }
            }
            if slider.auto {
                slideshow.slideshowInterval = Double(slider.speed / 1000)
            } else {
                slideshow.slideshowInterval = 0
            }
            slideshow.draggingEnabled = slider.manual
            slideshow.pageControlPosition = slider.items.count > 1 ? PageControlPosition.insideScrollView : PageControlPosition.hidden
            slideshow.pageControl.currentPageIndicatorTintColor = UIColor.colorAccent
            slideshow.pageControl.pageIndicatorTintColor = UIColor.white
            slideshow.contentScaleMode = UIViewContentMode.scaleAspectFill
            slideshow.setImageInputs(slider.items.map({ (item) -> AlamofireSource in
                return AlamofireSource(urlString: item.image.url)!
            }))
            slideshow.isHidden = false
            
        } else {
            slideshow.isHidden = true
        }
    }
    
    // MARK: Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == menuSegue) {
            let vc = segue.destination as! MenuViewController
            vc.menu = home.menu!
            menuVC = vc
        }
        if (segue.identifier == groupSegue) {
            let vc = segue.destination as! GroupViewController
            vc.groups = home.groups
            groupVC = vc
        }
        if segue.identifier == recentViewedSegue {
            let vc = segue.destination as! RecentViewedViewController
            recentViewedVC = vc
        }
    }
    
}

