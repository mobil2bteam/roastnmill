
import UIKit

class FavoriteViewController: UIViewController {

    @IBOutlet weak var itemsCollectionView: UICollectionView!
    let itemIdentifier = String(describing: ItemCollectionViewCell.self)
    var products: [CatalogModel]!
    var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(refreshProducts(sender:)), for: UIControlEvents.valueChanged)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Избранное"
        itemsCollectionView.backgroundColor = UIColor.colorBackgroundDark
        itemsCollectionView.register(ItemCollectionViewCell.nib(), forCellWithReuseIdentifier: itemIdentifier)
        itemsCollectionView.refreshControl = refreshControl
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if products == nil {
            loadFavoriteProducts()
        }
    }
    
    // MARK: Methods
    func loadFavoriteProducts() {
        self.refreshControl.beginRefreshing()
        itemsCollectionView.setContentOffset(CGPoint(x:0 , y:itemsCollectionView.contentOffset.y - refreshControl.frame.size.height), animated: true)
        let favoriteProducts = DataManager.getFavoriteProducts()
        if favoriteProducts.count > 0 {
            let stringId = favoriteProducts.map({ (productId) -> String in
                return "\(productId)-1"
            })
            let params = ["cart": stringId.joined(separator: ";")]
            OptionsManager.loadCart(params: params, successHandler: { (cart, error) in
                self.refreshControl.endRefreshing()
                if cart != nil {
                    self.products = cart!.products
                    self.itemsCollectionView.reloadData()
                }
                if error != nil {
                    self.alert(message: error!.localizedDescription)
                }
            })
        } else {
            self.products = nil
            self.refreshControl.endRefreshing()
            self.itemsCollectionView.reloadData()
        }
    }
    
    func refreshProducts(sender: AnyObject) {
        loadFavoriteProducts()
    }
    

    //MARK: Navigation
    func showProductViewController(for productId: Int) {
        let vc = Router.productViewController(productId: productId)
        navigationController?.pushViewController(vc, animated: true)
    }

}

extension FavoriteViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if products == nil {
            return 0
        }
        return products.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: itemIdentifier, for: indexPath) as! ItemCollectionViewCell
        cell.configure(for: products[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let product = products[indexPath.item]
        showProductViewController(for: product.id)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == itemsCollectionView {
            let width = collectionView.frame.size.width / 2.0 - 0.5
            return CGSize(width: width, height: ItemCollectionViewCell.itemHeight())
        } else {
            return (collectionViewLayout as! UICollectionViewFlowLayout).estimatedItemSize
        }
    }
}

