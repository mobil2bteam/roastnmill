
import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var deliveryView: UIView!
    
    let appManager = AppManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let user = appManager.currentUser {
            userLabel.text = user.info.userName
            emailLabel.text = user.info.email
        }
        deliveryView.isHidden = !SettingsManager.showDeliveryCity
    }
    
    @IBAction func exitButtonClicked(_ sender: Any) {
        appManager.currentUser = nil
        if Constants.appMode == .tabBar {
            // set main view controller as active tab
            tabBarController?.selectedIndex = 0
        } else {
            navigationController?.popToRootViewController(animated: true)
        }
    }

}
