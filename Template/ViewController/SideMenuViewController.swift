
import UIKit

class SideMenuViewController: UIViewController {

    @IBOutlet var buttonsArray: [UIButton]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for b in buttonsArray {
            b.setTitleColor(UIColor.colorTextNavMenu, for: .normal)
            b.tintColor = UIColor.colorIconNavMenu
        }
    }

    // MARK: Actions
    @IBAction func profileButtonClicked(_ sender: Any) {
        if AppManager.shared.currentUser == nil {
            let vc = UIStoryboard.init(name: "Login", bundle: nil).instantiateInitialViewController()! as! UINavigationController
            (vc.viewControllers[0] as! EnterViewController).completionBlock =  { [weak self] in
                self?.dismiss(animated: false, completion: nil)
                self?.performSegue(withIdentifier: "profileSegue", sender: nil)
            }
            present(vc, animated: false, completion: nil)
        } else {
            performSegue(withIdentifier: "profileSegue", sender: nil)
        }
    }

}
