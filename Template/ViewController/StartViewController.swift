
import UIKit
import SVProgressHUD

class StartViewController: UIViewController {

    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    // MARK: Methods

    func loadData() {
        SVProgressHUD.show()
        OptionsManager.loadOptions { [weak self] (error) in
            SVProgressHUD.dismiss()
            if (error != nil) {
                self?.alert(message: error!.localizedDescription, handler: { (action) in
                    self?.loadData()
                })
            } else {
                self?.prepare()
            }
        }
    }
    
    func prepare() {
        if Constants.appMode == .tabBar {
            let tabBarVC = MainTabBarViewController.init()
            present(tabBarVC, animated: true, completion: nil)
        } else {
            let vc = UIStoryboard.init(name: "Main", bundle: nil).instantiateInitialViewController()!
            UIApplication.shared.delegate!.window!!.rootViewController = vc
        }
    }

}
