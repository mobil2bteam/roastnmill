
import UIKit

class DescriptionViewController: UIViewController {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    var descriptionText: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Описание"
        descriptionLabel.attributedText = descriptionText.convertHtml()
        descriptionLabel.font = UIFont.systemFont(ofSize: 16)
    }
    
}
