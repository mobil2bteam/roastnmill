
import UIKit
import ImageSlideshow
import SVProgressHUD

class ProductViewController: UIViewController {
    var productId: Int!
    var product: ProductModel!
    @IBOutlet var slideshow: ImageSlideshow!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var errorLabel: UILabel!

    
    // MARK: Lifecycle
    convenience init(productId: Int) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.productId = productId
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        loadProduct()
    }

    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)
        if product != nil {
            title = product.name
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        title = ""
    }

    // MARK: Load data
    func loadProduct() {
        if productId != nil {
            SVProgressHUD.show()
            OptionsManager.getProduct(for: productId) { [weak self] (product, error) in
                SVProgressHUD.dismiss()
                if product != nil {
                    self?.product = product
                    self?.prepareView()
                    self?.errorView.isHidden = true
                }
                if error != nil {
                    self?.errorLabel.text = error!.localizedDescription
                }
            }
        }
    }

    // MARK: Preparation
    func prepareView() {
        title = product.name
        prepareLikeButton()
        nameLabel.text = product.name
        descriptionLabel.text = product.shortDescription
        DataManager.addProductToViewedProducts(product: product)
        prepareSliderView()
    }
    
    func prepareLikeButton() {
        if DataManager.favoriteProductIsAdded(productId: productId) {
            likeButton.setImage( UIImage.init(named: "like_fill")!, for: .normal)
        } else {
            likeButton.setImage( UIImage.init(named: "like")!, for: .normal)
        }
    }
    
    func prepareSliderView() {
        slideshow.draggingEnabled = true
        slideshow.pageControlPosition = PageControlPosition.insideScrollView
        slideshow.pageControl.currentPageIndicatorTintColor = UIColor.red
        slideshow.pageControl.pageIndicatorTintColor = UIColor.white
        slideshow.contentScaleMode = UIViewContentMode.scaleAspectFit
        slideshow.setImageInputs(product.images.map({ (image) -> AlamofireSource in
            return AlamofireSource(urlString: image.url)!
        }))
        slideshow.isHidden = false
    }

    
    // MARK: Actions
    @IBAction func cartButtonClicked(_ sender: Any) {
        CartManager.addProduct(product: product)
    }
    
    @IBAction func likeButtonClicked(_ sender: Any) {
        DataManager.favoriteProduct(productId: productId)
        prepareLikeButton()
    }

}
