
import UIKit
import SVProgressHUD

class OrderListViewController: HamburgerViewController {

    @IBOutlet weak var ordersTableView: UITableView!

    var refreshControl: UIRefreshControl!
    var orders: OrdersModel? {
        didSet {
            ordersTableView.reloadData()
        }
    }
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(loadOrders), for: .valueChanged)
        ordersTableView.refreshControl = refreshControl
        loadOrders()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Мои заказы"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        title = ""
    }

    // MARK: Load data
    func loadOrders() {
        refreshControl.beginRefreshing()
        ordersTableView.setContentOffset(CGPoint(x:0 , y:ordersTableView.contentOffset.y - refreshControl.frame.size.height), animated: true)
        OptionsManager.orders { [weak self] (orders, error) in
            SVProgressHUD.dismiss()
            self?.refreshControl.endRefreshing()
            if orders != nil {
                self?.orders = orders
            }
            if error != nil {
                self?.alert(message: error!)
            }
        }
    }

    
}


extension OrderListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders?.allOrders().count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderTableViewCell", for: indexPath) as! OrderTableViewCell
        cell.configure(for: orders!.allOrders()[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let order = orders!.allOrders()[indexPath.row]
        print(order.number)
    }
}
