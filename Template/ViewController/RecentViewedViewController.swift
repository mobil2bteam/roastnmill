
import UIKit

class RecentViewedViewController: UIViewController {

    @IBOutlet weak var itemsCollectionView: UICollectionView!
    @IBOutlet weak var itemsCollectionViewHeightConstraint: NSLayoutConstraint!
    let itemIdentifier = String(describing: ItemCollectionViewCell.self)
    let observationKey = "itemsCollectionView.contentSize"
    var products: [CatalogModel]!

    override func viewDidLoad() {
        super.viewDidLoad()
        view.translatesAutoresizingMaskIntoConstraints = false
        itemsCollectionView.backgroundColor = UIColor.colorBackgroundDark
        itemsCollectionView.register(ItemCollectionViewCell.nib(), forCellWithReuseIdentifier: itemIdentifier)
        addObserver(self, forKeyPath: observationKey, options: .new, context: nil)
        loadProducts()
    }

    // MARK: Lifecycle
    deinit {
        removeObserver(self, forKeyPath: observationKey)
    }

    // MARK: Notifcation observers
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == observationKey {
            var height = itemsCollectionView.collectionViewLayout.collectionViewContentSize.height
            if let _layout = itemsCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                if _layout.scrollDirection == .horizontal {
                    height = ItemCollectionViewCell.itemHeight()
                }
            }
            if itemsCollectionViewHeightConstraint.constant != height {
                itemsCollectionViewHeightConstraint.constant = height
                DispatchQueue.main.async { [weak self] in
                    self?.view.layoutIfNeeded()
                }
            }
        }
    }

    // MARK: Methods
    func reloadProducts() {
        loadProducts()
    }
    
    func loadProducts() {
        let favoriteProducts = DataManager.getViewedProducts()
        if favoriteProducts.count > 0 {
            let stringId = favoriteProducts.map({ (productId) -> String in
                return "\(productId)-1"
            })
            let params = ["cart": stringId.joined(separator: ";")]
            OptionsManager.loadCart(params: params, successHandler: { (cart, error) in
                if cart != nil {
                    self.products = cart!.products
                    self.itemsCollectionView.reloadData()
                }
                if error != nil {
                    self.alert(message: error!.localizedDescription)
                }
            })
        } else {
            self.itemsCollectionView.reloadData()
        }
    }
    
    //MARK: Navigation
    func showProductViewController(for productId: Int) {
        let vc = Router.productViewController(productId: productId)
        navigationController?.pushViewController(vc, animated: true)
    }

}


extension RecentViewedViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if products == nil {
            return 0
        }
        return products.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: itemIdentifier, for: indexPath) as! ItemCollectionViewCell
        cell.configure(for: products[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let product = products[indexPath.item]
        showProductViewController(for: product.id)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == itemsCollectionView {
            let width = collectionView.frame.size.width / 2
            return CGSize(width: width, height: ItemCollectionViewCell.itemHeight())
        } else {
            return (collectionViewLayout as! UICollectionViewFlowLayout).estimatedItemSize
        }
    }
}

