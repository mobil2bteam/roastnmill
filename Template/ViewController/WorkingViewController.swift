
import UIKit

class WorkingViewController: UIViewController {

    var working: [WorkModel] = []
    let identifier = String(describing: WorkingTableViewCell.self)

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "График работы"
    }

}

extension WorkingViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return working.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! WorkingTableViewCell
        cell.configure(for: working[indexPath.row])
        return cell
    }
    
}
