
import UIKit
import SVProgressHUD
import AlamofireImage

class MenuInfoViewController: UIViewController {

    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var descriptionDetailLabel: UILabel!
    @IBOutlet weak var descriptionShortLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptionShortView: BackgroundView!
    @IBOutlet weak var imageContentView: UIView!
    @IBOutlet weak var webView: UIWebView!

    var menuId: Int!
    var menuInfo: MenuInfoModel!
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        loadMenu()
    }

    // MARK: Methods
    func loadMenu() {
        if menuId != nil {
            SVProgressHUD.show()
            OptionsManager.getMenuInfo(for: menuId, successHandler: { (menuInfo, error) in
                SVProgressHUD.dismiss()
                if menuInfo != nil {
                    self.menuInfo = menuInfo
                    self.prepareView()
                }
                if error != nil {
                    self.errorLabel.text = error
                }
            })
        }
    }
    
    func prepareView() {
        title = menuInfo.name
        if let url = URL(string: menuInfo.url ?? "") {
            webView.loadRequest(URLRequest.init(url: url))
        } else {
            webView.isHidden = true
            if menuInfo.descriptionShort.isEmpty {
                descriptionShortView.isHidden = true
            } else {
                descriptionShortLabel.text = menuInfo.descriptionShort
            }
            let font = descriptionDetailLabel.font
            descriptionDetailLabel.attributedText = menuInfo.descriptionLong.convertHtml()
            descriptionDetailLabel.font = font
            descriptionDetailLabel.textColor = UIColor.black
            if let image = menuInfo.image, let url = URL(string: image.url) {
                imageView.af_setImage(withURL: url)
                let multiplier = image.aspectRatio
                imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: multiplier).isActive = true
                DispatchQueue.main.async { [weak self] in
                    self?.view.layoutIfNeeded()
                }
                imageContentView.isHidden = false
            }
        }
        errorView.isHidden = true
    }
}
