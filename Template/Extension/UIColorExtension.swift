

import UIKit

/**
 MissingHashMarkAsPrefix:   "Invalid RGB string, missing '#' as prefix"
 UnableToScanHexValue:      "Scan hex error"
 MismatchedHexStringLength: "Invalid RGB string, number of characters after '#' should be either 3, 4, 6 or 8"
 */
public enum UIColorInputError : Error {
    case missingHashMarkAsPrefix,
    unableToScanHexValue,
    mismatchedHexStringLength,
    unableToOutputHexStringForWideDisplayColor
}

extension UIColor {
    /**
     The shorthand three-digit hexadecimal representation of color.
     #RGB defines to the color #RRGGBB.
     
     - parameter hex3: Three-digit hexadecimal value.
     - parameter alpha: 0.0 - 1.0. The default is 1.0.
     */
    public convenience init(hex3: UInt16, alpha: CGFloat = 1) {
        let divisor = CGFloat(15)
        let red     = CGFloat((hex3 & 0xF00) >> 8) / divisor
        let green   = CGFloat((hex3 & 0x0F0) >> 4) / divisor
        let blue    = CGFloat( hex3 & 0x00F      ) / divisor
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    /**
     The shorthand four-digit hexadecimal representation of color with alpha.
     #RGBA defines to the color #RRGGBBAA.
     
     - parameter hex4: Four-digit hexadecimal value.
     */
    public convenience init(hex4: UInt16) {
        let divisor = CGFloat(15)
        let red     = CGFloat((hex4 & 0xF000) >> 12) / divisor
        let green   = CGFloat((hex4 & 0x0F00) >>  8) / divisor
        let blue    = CGFloat((hex4 & 0x00F0) >>  4) / divisor
        let alpha   = CGFloat( hex4 & 0x000F       ) / divisor
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    /**
     The six-digit hexadecimal representation of color of the form #RRGGBB.
     
     - parameter hex6: Six-digit hexadecimal value.
     */
    public convenience init(hex6: UInt32, alpha: CGFloat = 1) {
        let divisor = CGFloat(255)
        let red     = CGFloat((hex6 & 0xFF0000) >> 16) / divisor
        let green   = CGFloat((hex6 & 0x00FF00) >>  8) / divisor
        let blue    = CGFloat( hex6 & 0x0000FF       ) / divisor
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    /**
     The six-digit hexadecimal representation of color with alpha of the form #RRGGBBAA.
     
     - parameter hex8: Eight-digit hexadecimal value.
     */
    public convenience init(hex8: UInt32) {
        let divisor = CGFloat(255)
        let red     = CGFloat((hex8 & 0xFF000000) >> 24) / divisor
        let green   = CGFloat((hex8 & 0x00FF0000) >> 16) / divisor
        let blue    = CGFloat((hex8 & 0x0000FF00) >>  8) / divisor
        let alpha   = CGFloat( hex8 & 0x000000FF       ) / divisor
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    /**
     The rgba string representation of color with alpha of the form #RRGGBBAA/#RRGGBB, throws error.
     
     - parameter rgba: String value.
     */
    public convenience init(rgba_throws rgba: String) throws {
        guard rgba.hasPrefix("#") else {
            throw UIColorInputError.missingHashMarkAsPrefix
        }
        
        let hexString: String = rgba.substring(from: rgba.characters.index(rgba.startIndex, offsetBy: 1))
        var hexValue:  UInt32 = 0
        
        guard Scanner(string: hexString).scanHexInt32(&hexValue) else {
            throw UIColorInputError.unableToScanHexValue
        }
        
        switch (hexString.characters.count) {
        case 3:
            self.init(hex3: UInt16(hexValue))
        case 4:
            self.init(hex4: UInt16(hexValue))
        case 6:
            self.init(hex6: hexValue)
        case 8:
            self.init(hex8: hexValue)
        default:
            throw UIColorInputError.mismatchedHexStringLength
        }
    }
    
    /**
     The rgba string representation of color with alpha of the form #RRGGBBAA/#RRGGBB, fails to default color.
     
     - parameter rgba: String value.
     */
    public convenience init(_ rgba: String, defaultColor: UIColor = UIColor.clear) {
        guard let color = try? UIColor(rgba_throws: rgba) else {
            self.init(cgColor: defaultColor.cgColor)
            return
        }
        self.init(cgColor: color.cgColor)
    }
    
    /**
     Hex string of a UIColor instance, throws error.
     
     - parameter includeAlpha: Whether the alpha should be included.
     */
    public func hexStringThrows(_ includeAlpha: Bool = true) throws -> String  {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        guard r >= 0 && r <= 1 && g >= 0 && g <= 1 && b >= 0 && b <= 1 else {
            throw UIColorInputError.unableToOutputHexStringForWideDisplayColor
        }
        
        if (includeAlpha) {
            return String(format: "#%02X%02X%02X%02X", Int(r * 255), Int(g * 255), Int(b * 255), Int(a * 255))
        } else {
            return String(format: "#%02X%02X%02X", Int(r * 255), Int(g * 255), Int(b * 255))
        }
    }
    
    /**
     Hex string of a UIColor instance, fails to empty string.
     
     - parameter includeAlpha: Whether the alpha should be included.
     */
    public func hexString(_ includeAlpha: Bool = true) -> String  {
        guard let hexString = try? hexStringThrows(includeAlpha) else {
            return ""
        }
        return hexString
    }
}

extension String {
    /**
     Convert argb string to rgba string.
     */
    public func argb2rgba() -> String? {
        guard self.hasPrefix("#") else {
            return nil
        }
        
        let hexString: String = self.substring(from: self.characters.index(self.startIndex, offsetBy: 1))
        switch (hexString.characters.count) {
        case 4:
            return "#"
                + hexString.substring(from: self.characters.index(self.startIndex, offsetBy: 1))
                + hexString.substring(to: self.characters.index(self.startIndex, offsetBy: 1))
        case 8:
            return "#"
                + hexString.substring(from: self.characters.index(self.startIndex, offsetBy: 2))
                + hexString.substring(to: self.characters.index(self.startIndex, offsetBy: 2))
        default:
            return nil
        }
    }
}

extension String{
    
    func convertHtml() -> NSAttributedString{
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do{
            return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }catch{
            return NSAttributedString()
        }
    }
    
}

extension UIColor {
    
    // Основной цвет приложения
    open class var colorPrimary: UIColor {
        get {
            return UIColor("#9EDE95")
        }
    }
    
    // Основной темный цвет приложения
    open class var colorPrimaryDark: UIColor {
        get {
            return UIColor("#8CC983")
        }
    }
    
    // Основной акцент цвет приложения
    open class var colorAccent: UIColor {

        get {
            return UIColor("#ed565b")
        }
    }
    
    // Основной цвет фона
    open class var colorBackground: UIColor {
        get {
            return UIColor("#ffffff")
        }
    }
    
    // Основной цвет темного фона, разделительные линии
    open class var colorBackgroundDark: UIColor {
        get {
            return UIColor("#f3f3f3")
        }
    }

    // Цвет фона заголовка
    open class var colorBackgroundCaptionGroupView: UIColor {
        get {
            return UIColor("#ffffff")
        }
    }

    // Цвет фона заголовка
    open class var colorBackgroundToolbar: UIColor {
        get {
            return UIColor("#ffffff")
        }
    }

    // Цвет флага скидки
    open class var colorBackgroundFlagDiscount: UIColor {
        get {
            return UIColor("#ED565B")
        }
    }

    // Цвет фона основного меню
    open class var colorBackgroundNav: UIColor {
        get {
            return UIColor("#f3f3f3")
        }
    }

    // Стандартный цвет текста во всем проекте
    open class var colorTextPrimary: UIColor {
        get {
            return UIColor("#606277")
        }
    }

    // Вспомогательный цвет текста во всем проекте
    open class var colorTextSecond: UIColor {
        get {
            return UIColor("#878686")
        }
    }

    // Акцент цвет текста во всем проекте
    open class var colorTextAccent: UIColor {
        get {
            return UIColor("#8CC983")
        }
    }

    // Супер Акцент цвет текста во всем проекте
    open class var colorTextSuperAccent: UIColor {
        get {
            return UIColor("#ED565B")
        }
    }
    
    // Белый цвет текста
    open class var colorTextWhite: UIColor {
        get {
            return UIColor("#ffffff")
        }
    }
    
     // Цвет текста в пунктах меню
    open class var colorTextNavMenu: UIColor {
        get {
            return UIColor("#606277")
        }
    }

    // Цвет цены товара
    open class var colorTextPrice: UIColor {
        get {
            return UIColor("#606277")
        }
    }
    
    // Цвет цены со скидкой
    open class var colorTextDiscountPrice: UIColor {
        get {
            return UIColor("#ed565b")
        }
    }

     // Цвет старой цены
    open class var colorTextOldPrice: UIColor {
        get {
            return UIColor("#878686")
        }
    }

    // Цвет процента скидки на бирке со скидкой
    open class var colorTextFlagDiscount: UIColor {
        get {
            return UIColor("#ffffff")
        }
    }

    // Цвет текста заголовка на подборке товара
    open class var colorTextCaptionGroupView: UIColor {
        get {
            return UIColor("#606277")
        }
    }

     // Стандартный цвет текста загловка
    open class var colorTextToolbarTitle: UIColor {
        get {
            return UIColor("#606277")
        }
    }

    // Стандартный цвет вспомогательного текста загловка
    open class var colorTextToolbarSecond: UIColor {
        get {
            return UIColor("#878686")
        }
    }

     // Цвет фона баллов, если рейтинг выше 4
    open class var colorTextRating4: UIColor {
        get {
            return UIColor("#8CC983")
        }
    }

    // Цвет фона баллов, если рейтинг выше 3
    open class var colorTextRating3: UIColor {
        get {
            return UIColor("#606277")
        }
    }

    // Цвет фона баллов, если рейтинг ниже 3
    open class var colorTextRating2: UIColor {
        get {
            return UIColor("#ed565b")
        }
    }

    // Стандартный цвет текста на кнопке
    open class var colorButtonTextDefault: UIColor {
        get {
            return UIColor("#ffffff")
        }
    }
    
    // Стандартный цвет текста на кнопке типа link
    open class var colorButtonTextPrimaryLink: UIColor {
        get {
            return UIColor("#9EDE95")
        }
    }
    
    // Стандартный цвет текста на кнопке типа link, accent стиля
    open class var colorButtonTextAccentLink: UIColor {
        get {
            return UIColor("#ED565B")
        }
    }
    
    // Стандартный цвет кнопки
    open class var colorButtonPrimary: UIColor {
        get {
            return UIColor("#8CC983")
        }
    }
    
    // Стандартный цвет при нажатии
    open class var colorButtonPrimaryLite: UIColor {
        get {
            return UIColor("#8CC983")
        }
    }
    
    // Стандартный цвет кнопки accent
    open class var colorButtonAccent: UIColor {
        get {
            return UIColor("#ED565B")
        }
    }

    // Стандартный цвет кнопки accent при нажатии
    open class var colorButtonAccentLite: UIColor {
        get {
            return UIColor("#ED565B")
        }
    }
    
    // Стандартный цвет всех иконок в приложении
    open class var colorIconPrimary: UIColor {
        get {
            return UIColor("#606277")
        }
    }
    
    // Стандартный цвет выделенной иконки
    open class var colorIconSelect: UIColor {
        get {
            return UIColor("#FFFFFF")
        }
    }
    
    // Стандартный акцент цвет иконки
    open class var colorIconAccent: UIColor {
        get {
            return UIColor("#8CC983")
        }
    }
    
    // Стандартный цвет иконок в меню навигации
    open class var colorIconNavMenu: UIColor {
        get {
            return UIColor("#606277")
        }
    }

}
