

import Foundation


class SearchingManager {
    
    static let searchingKey = "DataManager.favoriteProducts"
    
    class func addText(text: String) {
        let userDefaults = UserDefaults.standard
        if var temp = userDefaults.array(forKey: searchingKey) as? [String] {
            if let index = temp.index(of: text) {
                temp.remove(at: index)
            }
            temp.append(text)
            userDefaults.set(temp, forKey: searchingKey)
        } else {
            userDefaults.set([text], forKey: searchingKey)
        }
        userDefaults.synchronize()
    }
    
    class func getValues() -> [String] {
        let userDefaults = UserDefaults.standard
        if let temp = userDefaults.array(forKey: searchingKey) as? [String] {
            return temp.reversed()
        } else {
            return []
        }
    }
    
    class func clear() {
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: searchingKey)
        userDefaults.synchronize()
    }
    
}
