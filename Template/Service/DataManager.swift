
import Foundation
import RealmSwift

let uiRealm = try! Realm()

class DataManager {
    
    static let productsCountLimit = 30
    static let viewedProductsKey = "DataManager.viewedProducts"
    static let favoriteProductsKey = "DataManager.favoriteProducts"

    class func addProductToViewedProducts(product: ProductModel) {
        let userDefaults = UserDefaults.standard
        if var products = userDefaults.array(forKey: viewedProductsKey) as? [Int] {
            if let index = products.index(of: product.id) {
                products.remove(at: index)
            }
            if products.count > productsCountLimit {
                products.remove(at: 0)
            } else {
                products.append(product.id)
                userDefaults.set(products, forKey: viewedProductsKey)
                print(products)
            }
        } else {
            userDefaults.set([product.id], forKey: viewedProductsKey)
        }
        userDefaults.synchronize()
    }
    
    class func getViewedProducts() -> [Int] {
        let userDefaults = UserDefaults.standard
        if let products = userDefaults.array(forKey: viewedProductsKey) as? [Int] {
            return products.reversed()
        } else {
            return []
        }
    }

    // MARK: Favorite products
    class func getFavoriteProducts() -> [Int] {
        let userDefaults = UserDefaults.standard
        if let products = userDefaults.array(forKey: favoriteProductsKey) as? [Int] {
            return products.reversed()
        } else {
            return []
        }
    }

    class func favoriteProduct(productId: Int) {
        let userDefaults = UserDefaults.standard
        if var products = userDefaults.array(forKey: favoriteProductsKey) as? [Int] {
            if let index = products.index(of: productId) {
                products.remove(at: index)
            } else {
                products.append(productId)
            }
            if products.count > 0 {
                userDefaults.set(products, forKey: favoriteProductsKey)
            } else {
                userDefaults.removeObject(forKey: favoriteProductsKey)
            }
        } else {
            userDefaults.set([productId], forKey: favoriteProductsKey)
        }
        userDefaults.synchronize()
    }
    
    class func favoriteProductIsAdded(productId: Int) -> Bool {
        let userDefaults = UserDefaults.standard
        if let products = userDefaults.array(forKey: favoriteProductsKey) as? [Int] {
            return products.contains(productId)
        }
        return false
    }
    

}
