
import Foundation

class CityManager {
    
    let appManager = AppManager.shared
    static let notificationName = "CityManager.ChangeCity"
    static let cityKeyPath = "city"
    
    class func setDeliveryCity(city: CityModel) {
        UserDefaults.standard.setValue(city.id, forKey: cityKeyPath)
        UserDefaults.standard.synchronize()
        postNotification()
    }
    
    class func getDeliveryCity() -> CityModel {
        let cities = AppManager.shared.options.directory.cities!
        let searchCityID: Int
        if let id = UserDefaults.standard.value(forKey: cityKeyPath) {
            searchCityID = id as! Int
        } else {
            searchCityID = AppManager.shared.options.settings.defaultСity
        }
        let results = cities.filter {
            $0.id == searchCityID
        }
        if results.count > 0 {
            return results[0]
        }
        return cities[0]
    }
    
    private class func postNotification() {
        let notificationName = Notification.Name(self.notificationName)
        NotificationCenter.default.post(name: notificationName, object: nil)
    }
}
