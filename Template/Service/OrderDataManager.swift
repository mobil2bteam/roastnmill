
import Foundation
import RealmSwift

class OrderDataManager {
    
    static let lastOrderKey = "lastOrderWithRecipient"
    
    // MARK: Address
    class func addAddress(street: String, home: String, flat: String, comment: String = "", zipCode: String = "") -> AddressDataModel {
        let results = uiRealm.objects(AddressDataModel.self).filter("street = %@ && home = %@ && flat = %@", street, home, flat)
        if results.count > 0 {
            try! uiRealm.write{
                uiRealm.delete(results)
            }
        }
        let newAddress = AddressDataModel()
        newAddress.street = street
        newAddress.home = home
        newAddress.flat = flat
        newAddress.comment = comment
        newAddress.zipCode = zipCode
        let primaryKey = (uiRealm.objects(AddressDataModel.self).max(ofProperty: "id") as Int? ?? 0) + 1
        newAddress.id = primaryKey
        try! uiRealm.write{
            uiRealm.add(newAddress)
        }
        return newAddress
    }
    
    class func getAllAdresses() -> [AddressDataModel] {
        let results = uiRealm.objects(AddressDataModel.self).sorted(byKeyPath: "date", ascending: false)
        return Array(results)
    }
    
    class func removeAddress(address: AddressDataModel) {
        try! uiRealm.write{
            uiRealm.delete(address)
        }
    }
    
    class func showRecipientIfPossible() -> Bool {
        let userDefaults = UserDefaults.standard
        if let result = userDefaults.value(forKey: lastOrderKey) {
            if let boolValue = result as? Bool {
                return boolValue
            }
        }
        return true
    }
    
    class func setLastOrderWithRecipient(withRecipient: Bool) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(withRecipient, forKey: lastOrderKey)
        userDefaults.synchronize()
    }
    
    // MARK: Recipients
    class func addRecipient(name: String, phone: String) -> RecipientDataModel {
        let results = uiRealm.objects(RecipientDataModel.self).filter("name = %@ && phone = %@", name, phone)
        if results.count > 0 {
            try! uiRealm.write{
                uiRealm.delete(results)
            }
        }
        let newRecipient = RecipientDataModel()
        newRecipient.name = name
        newRecipient.phone = phone
        let primaryKey = (uiRealm.objects(RecipientDataModel.self).max(ofProperty: "id") as Int? ?? 0) + 1
        newRecipient.id = primaryKey
        try! uiRealm.write{
            uiRealm.add(newRecipient)
        }
        return newRecipient
    }
    
    class func getAllRecipients() -> [RecipientDataModel] {
        let results = uiRealm.objects(RecipientDataModel.self).sorted(byKeyPath: "date", ascending: false)
        return Array(results)
    }
    
    class func removeRecipient(recipient: RecipientDataModel) {
        try! uiRealm.write{
            uiRealm.delete(recipient)
        }
    }

    
}
